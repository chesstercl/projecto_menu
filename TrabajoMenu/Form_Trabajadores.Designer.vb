﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form_Trabajadores
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.ComboBox_Estado = New System.Windows.Forms.ComboBox()
        Me.Button_Eliminar = New System.Windows.Forms.Button()
        Me.Button_Modificar = New System.Windows.Forms.Button()
        Me.Button_Agregar = New System.Windows.Forms.Button()
        Me.Button_Buscar = New System.Windows.Forms.Button()
        Me.NumericUD_SueldoBase = New System.Windows.Forms.NumericUpDown()
        Me.TextBox_Nombre = New System.Windows.Forms.TextBox()
        Me.TextBox_Rut = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.DataGridView_Trabajadores = New System.Windows.Forms.DataGridView()
        Me.GroupBox1.SuspendLayout()
        CType(Me.NumericUD_SueldoBase, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.DataGridView_Trabajadores, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.ComboBox_Estado)
        Me.GroupBox1.Controls.Add(Me.Button_Eliminar)
        Me.GroupBox1.Controls.Add(Me.Button_Modificar)
        Me.GroupBox1.Controls.Add(Me.Button_Agregar)
        Me.GroupBox1.Controls.Add(Me.Button_Buscar)
        Me.GroupBox1.Controls.Add(Me.NumericUD_SueldoBase)
        Me.GroupBox1.Controls.Add(Me.TextBox_Nombre)
        Me.GroupBox1.Controls.Add(Me.TextBox_Rut)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 13)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(259, 209)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos Personales"
        '
        'ComboBox_Estado
        '
        Me.ComboBox_Estado.FormattingEnabled = True
        Me.ComboBox_Estado.Location = New System.Drawing.Point(91, 122)
        Me.ComboBox_Estado.Name = "ComboBox_Estado"
        Me.ComboBox_Estado.Size = New System.Drawing.Size(145, 21)
        Me.ComboBox_Estado.TabIndex = 13
        '
        'Button_Eliminar
        '
        Me.Button_Eliminar.Location = New System.Drawing.Point(173, 171)
        Me.Button_Eliminar.Name = "Button_Eliminar"
        Me.Button_Eliminar.Size = New System.Drawing.Size(63, 23)
        Me.Button_Eliminar.TabIndex = 12
        Me.Button_Eliminar.Text = "Eliminar"
        Me.Button_Eliminar.UseVisualStyleBackColor = True
        '
        'Button_Modificar
        '
        Me.Button_Modificar.Location = New System.Drawing.Point(91, 171)
        Me.Button_Modificar.Name = "Button_Modificar"
        Me.Button_Modificar.Size = New System.Drawing.Size(76, 23)
        Me.Button_Modificar.TabIndex = 11
        Me.Button_Modificar.Text = "Modificar"
        Me.Button_Modificar.UseVisualStyleBackColor = True
        '
        'Button_Agregar
        '
        Me.Button_Agregar.Location = New System.Drawing.Point(21, 171)
        Me.Button_Agregar.Name = "Button_Agregar"
        Me.Button_Agregar.Size = New System.Drawing.Size(64, 23)
        Me.Button_Agregar.TabIndex = 10
        Me.Button_Agregar.Text = "Agregar"
        Me.Button_Agregar.UseVisualStyleBackColor = True
        '
        'Button_Buscar
        '
        Me.Button_Buscar.Location = New System.Drawing.Point(173, 20)
        Me.Button_Buscar.Name = "Button_Buscar"
        Me.Button_Buscar.Size = New System.Drawing.Size(63, 23)
        Me.Button_Buscar.TabIndex = 9
        Me.Button_Buscar.Text = "Buscar"
        Me.Button_Buscar.UseVisualStyleBackColor = True
        '
        'NumericUD_SueldoBase
        '
        Me.NumericUD_SueldoBase.Location = New System.Drawing.Point(91, 80)
        Me.NumericUD_SueldoBase.Name = "NumericUD_SueldoBase"
        Me.NumericUD_SueldoBase.Size = New System.Drawing.Size(145, 20)
        Me.NumericUD_SueldoBase.TabIndex = 8
        '
        'TextBox_Nombre
        '
        Me.TextBox_Nombre.Location = New System.Drawing.Point(91, 51)
        Me.TextBox_Nombre.Name = "TextBox_Nombre"
        Me.TextBox_Nombre.Size = New System.Drawing.Size(145, 20)
        Me.TextBox_Nombre.TabIndex = 5
        '
        'TextBox_Rut
        '
        Me.TextBox_Rut.Location = New System.Drawing.Point(91, 20)
        Me.TextBox_Rut.MaxLength = 12
        Me.TextBox_Rut.Name = "TextBox_Rut"
        Me.TextBox_Rut.Size = New System.Drawing.Size(76, 20)
        Me.TextBox_Rut.TabIndex = 4
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(22, 130)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(40, 13)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Estado"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(22, 87)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(67, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Sueldo Base"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(22, 58)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(44, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Nombre"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(22, 20)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(24, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Rut"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.DataGridView_Trabajadores)
        Me.GroupBox2.Location = New System.Drawing.Point(277, 13)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(560, 341)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Trabajadores"
        '
        'DataGridView_Trabajadores
        '
        Me.DataGridView_Trabajadores.AllowUserToAddRows = False
        Me.DataGridView_Trabajadores.AllowUserToDeleteRows = False
        Me.DataGridView_Trabajadores.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView_Trabajadores.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView_Trabajadores.Location = New System.Drawing.Point(3, 16)
        Me.DataGridView_Trabajadores.Name = "DataGridView_Trabajadores"
        Me.DataGridView_Trabajadores.ReadOnly = True
        Me.DataGridView_Trabajadores.Size = New System.Drawing.Size(554, 322)
        Me.DataGridView_Trabajadores.TabIndex = 0
        '
        'Form_Trabajadores
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(851, 371)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "Form_Trabajadores"
        Me.Text = "Trabajadores"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.NumericUD_SueldoBase, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.DataGridView_Trabajadores, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents NumericUD_SueldoBase As NumericUpDown
    Friend WithEvents TextBox_Nombre As TextBox
    Friend WithEvents TextBox_Rut As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents Button_Buscar As Button
    Friend WithEvents Button_Eliminar As Button
    Friend WithEvents Button_Modificar As Button
    Friend WithEvents Button_Agregar As Button
    Friend WithEvents DataGridView_Trabajadores As DataGridView
    Friend WithEvents ComboBox_Estado As ComboBox
End Class
