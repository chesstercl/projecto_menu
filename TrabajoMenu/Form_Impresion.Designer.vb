﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form_Impresion
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form_Impresion))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TextBox_Periodo = New System.Windows.Forms.TextBox()
        Me.Button_Obtener = New System.Windows.Forms.Button()
        Me.ListView1 = New System.Windows.Forms.ListView()
        Me.ColumnHeader_Nombre = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader_Rut = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader_SueldoBase = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader_Bonos = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader_Descuentos = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader_SueldoLiquido = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Button_Print = New System.Windows.Forms.Button()
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument()
        Me.PrintPreviewDialog1 = New System.Windows.Forms.PrintPreviewDialog()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(141, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(163, 25)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "IMPRESIÓN DE"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 55)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(135, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Buscar período (AAAAMM)"
        '
        'TextBox_Periodo
        '
        Me.TextBox_Periodo.Location = New System.Drawing.Point(153, 48)
        Me.TextBox_Periodo.Name = "TextBox_Periodo"
        Me.TextBox_Periodo.Size = New System.Drawing.Size(63, 20)
        Me.TextBox_Periodo.TabIndex = 2
        '
        'Button_Obtener
        '
        Me.Button_Obtener.Location = New System.Drawing.Point(231, 45)
        Me.Button_Obtener.Name = "Button_Obtener"
        Me.Button_Obtener.Size = New System.Drawing.Size(526, 23)
        Me.Button_Obtener.TabIndex = 3
        Me.Button_Obtener.Text = "Obtener"
        Me.Button_Obtener.UseVisualStyleBackColor = True
        '
        'ListView1
        '
        Me.ListView1.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader_Nombre, Me.ColumnHeader_Rut, Me.ColumnHeader_SueldoBase, Me.ColumnHeader_Bonos, Me.ColumnHeader_Descuentos, Me.ColumnHeader_SueldoLiquido})
        Me.ListView1.GridLines = True
        Me.ListView1.Location = New System.Drawing.Point(13, 72)
        Me.ListView1.Name = "ListView1"
        Me.ListView1.Size = New System.Drawing.Size(612, 278)
        Me.ListView1.TabIndex = 4
        Me.ListView1.UseCompatibleStateImageBehavior = False
        Me.ListView1.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader_Nombre
        '
        Me.ColumnHeader_Nombre.Text = "Nombre"
        Me.ColumnHeader_Nombre.Width = 160
        '
        'ColumnHeader_Rut
        '
        Me.ColumnHeader_Rut.Text = "Rut"
        Me.ColumnHeader_Rut.Width = 76
        '
        'ColumnHeader_SueldoBase
        '
        Me.ColumnHeader_SueldoBase.Text = "Sueldo Base"
        Me.ColumnHeader_SueldoBase.Width = 87
        '
        'ColumnHeader_Bonos
        '
        Me.ColumnHeader_Bonos.Text = "Bonos"
        '
        'ColumnHeader_Descuentos
        '
        Me.ColumnHeader_Descuentos.Text = "Descuentos"
        Me.ColumnHeader_Descuentos.Width = 85
        '
        'ColumnHeader_SueldoLiquido
        '
        Me.ColumnHeader_SueldoLiquido.Text = "Sueldo Liquido"
        Me.ColumnHeader_SueldoLiquido.Width = 94
        '
        'Button_Print
        '
        Me.Button_Print.Location = New System.Drawing.Point(659, 327)
        Me.Button_Print.Name = "Button_Print"
        Me.Button_Print.Size = New System.Drawing.Size(75, 23)
        Me.Button_Print.TabIndex = 5
        Me.Button_Print.Text = "Imprimir"
        Me.Button_Print.UseVisualStyleBackColor = True
        '
        'PrintDocument1
        '
        '
        'PrintPreviewDialog1
        '
        Me.PrintPreviewDialog1.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.PrintPreviewDialog1.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.PrintPreviewDialog1.ClientSize = New System.Drawing.Size(400, 300)
        Me.PrintPreviewDialog1.Enabled = True
        Me.PrintPreviewDialog1.Icon = CType(resources.GetObject("PrintPreviewDialog1.Icon"), System.Drawing.Icon)
        Me.PrintPreviewDialog1.Name = "PrintPreviewDialog1"
        Me.PrintPreviewDialog1.Visible = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(298, 9)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(314, 25)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "LIQUIDACIONES DE SUELDOS"
        '
        'Form_Impresion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(769, 389)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Button_Print)
        Me.Controls.Add(Me.ListView1)
        Me.Controls.Add(Me.Button_Obtener)
        Me.Controls.Add(Me.TextBox_Periodo)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Name = "Form_Impresion"
        Me.Text = "Impresion"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents TextBox_Periodo As TextBox
    Friend WithEvents Button_Obtener As Button
    Friend WithEvents ListView1 As ListView
    Friend WithEvents ColumnHeader_Nombre As ColumnHeader
    Friend WithEvents ColumnHeader_Rut As ColumnHeader
    Friend WithEvents ColumnHeader_SueldoBase As ColumnHeader
    Friend WithEvents ColumnHeader_Bonos As ColumnHeader
    Friend WithEvents ColumnHeader_Descuentos As ColumnHeader
    Friend WithEvents ColumnHeader_SueldoLiquido As ColumnHeader
    Friend WithEvents Button_Print As Button
    Friend WithEvents PrintDocument1 As Printing.PrintDocument
    Friend WithEvents PrintPreviewDialog1 As PrintPreviewDialog
    Friend WithEvents Label3 As Label
End Class
