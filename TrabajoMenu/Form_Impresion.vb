﻿Imports System.Drawing.Printing

Public Class Form_Impresion

    Dim HH As Integer
    Dim Linenumber As Integer
    Dim Linerpage As Integer
    Dim I_Start As Integer
    Dim I_Counter As Integer

    Private Sub Button_Obtener_Click(sender As Object, e As EventArgs) Handles Button_Obtener.Click

        Periodo(TextBox_Periodo.Text)
    End Sub

    Private Sub Form_Impresion_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Button_Obtener.Enabled = False
        Linerpage = 25
    End Sub


    Private Sub TextBox_Periodo_TextChanged(sender As Object, e As EventArgs) Handles TextBox_Periodo.TextChanged
        If Len(TextBox_Periodo.Text) <> 6 Then
            Button_Obtener.Enabled = False
        Else
            Button_Obtener.Enabled = True
        End If
    End Sub

    Private Sub Button_Print_Click(sender As Object, e As EventArgs) Handles Button_Print.Click
        PrintPreviewDialog1.Document = PrintDocument1
        If PrintPreviewDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            PrintPreviewDialog1.Show()
            Dim New_Paper As New PaperSize("", 800, 800) With {
                .PaperName = PaperKind.Custom
            }
            Dim New_Margin As New Margins With {
                .Left = 0,
                .Top = 50
            }

            With PrintDocument1
                .DefaultPageSettings.PaperSize = New_Paper
                .DefaultPageSettings.Margins = New_Margin
                .PrinterSettings.DefaultPageSettings.Landscape = False
            End With

        End If
    End Sub

    Private Sub PrintDocument1_PrintPage(sender As Object, e As PrintPageEventArgs) Handles PrintDocument1.PrintPage
        HH = 50
        e.Graphics.DrawString(Label3.Text, New Font("Arial", 24), Brushes.Black, 150, HH)
        HH += 50
        e.Graphics.DrawString("Nombre", New Font("Couriee New", 12), Brushes.Black, 150, HH)
        e.Graphics.DrawString("Rut", New Font("Couriee New", 12), Brushes.Black, 250, HH)
        e.Graphics.DrawString("Sueldo Base", New Font("Couriee New", 12), Brushes.Black, 350, HH)
        e.Graphics.DrawString("Bonos", New Font("Couriee New", 12), Brushes.Black, 475, HH)
        e.Graphics.DrawString("Descuentos", New Font("Couriee New", 12), Brushes.Black, 550, HH)
        e.Graphics.DrawString("Sueldo Liquido", New Font("Couriee New", 12), Brushes.Black, 650, HH)
        HH += 30
        Dim NN As Integer = HH
        e.Graphics.DrawLine(Pens.Black, 150, NN, 800, NN)

        For Me.I_Counter = I_Start To ListView1.Items.Count - 1
            e.Graphics.DrawString(ListView1.Items(I_Counter).Text, New Font("Couriee New", 12), Brushes.Black, 150, HH)
            e.Graphics.DrawString(ListView1.Items(I_Counter).SubItems(1).Text, New Font("Couriee New", 12), Brushes.Black, 250, HH)
            e.Graphics.DrawString(ListView1.Items(I_Counter).SubItems(2).Text, New Font("Couriee New", 12), Brushes.Black, 350, HH)
            e.Graphics.DrawString(ListView1.Items(I_Counter).SubItems(3).Text, New Font("Couriee New", 12), Brushes.Black, 475, HH)
            e.Graphics.DrawString(ListView1.Items(I_Counter).SubItems(4).Text, New Font("Couriee New", 12), Brushes.Black, 550, HH)
            e.Graphics.DrawString(ListView1.Items(I_Counter).SubItems(5).Text, New Font("Couriee New", 12), Brushes.Black, 650, HH)
            HH += 30
            Linenumber += 1
            If Linenumber = Linerpage Then
                Linenumber = 0
                I_Start = I_Counter + 1
                HH = 50
                e.HasMorePages = True
                Exit For
            End If
        Next
    End Sub
End Class