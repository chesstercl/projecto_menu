﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form_RRHH
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Button_ObtenerDatosLiquidacion = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TextBox_Periodo = New System.Windows.Forms.TextBox()
        Me.ComboBox_Nombre = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Button_Modificar = New System.Windows.Forms.Button()
        Me.Button_Eliminar = New System.Windows.Forms.Button()
        Me.Button_Agregar = New System.Windows.Forms.Button()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TextBox_SueldoLiquido = New System.Windows.Forms.TextBox()
        Me.Button_CalcularSueldoLiquido = New System.Windows.Forms.Button()
        Me.TextBox_Descuentos = New System.Windows.Forms.TextBox()
        Me.TextBox_Bonos = New System.Windows.Forms.TextBox()
        Me.TextBox_SueldoBase = New System.Windows.Forms.TextBox()
        Me.TextBox_Rut = New System.Windows.Forms.TextBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.DataGridView_Liquidaciones = New System.Windows.Forms.DataGridView()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.DataGridView_Liquidaciones, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Button_ObtenerDatosLiquidacion)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.TextBox_Periodo)
        Me.GroupBox1.Controls.Add(Me.ComboBox_Nombre)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(13, 13)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(245, 100)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos Trabajador"
        '
        'Button_ObtenerDatosLiquidacion
        '
        Me.Button_ObtenerDatosLiquidacion.Location = New System.Drawing.Point(30, 63)
        Me.Button_ObtenerDatosLiquidacion.Name = "Button_ObtenerDatosLiquidacion"
        Me.Button_ObtenerDatosLiquidacion.Size = New System.Drawing.Size(209, 23)
        Me.Button_ObtenerDatosLiquidacion.TabIndex = 4
        Me.Button_ObtenerDatosLiquidacion.Text = "Obtener Datos Liquidación"
        Me.Button_ObtenerDatosLiquidacion.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(178, 20)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(43, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Periodo"
        '
        'TextBox_Periodo
        '
        Me.TextBox_Periodo.Location = New System.Drawing.Point(178, 37)
        Me.TextBox_Periodo.MaxLength = 6
        Me.TextBox_Periodo.Name = "TextBox_Periodo"
        Me.TextBox_Periodo.Size = New System.Drawing.Size(61, 20)
        Me.TextBox_Periodo.TabIndex = 2
        '
        'ComboBox_Nombre
        '
        Me.ComboBox_Nombre.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox_Nombre.FormattingEnabled = True
        Me.ComboBox_Nombre.Location = New System.Drawing.Point(30, 36)
        Me.ComboBox_Nombre.Name = "ComboBox_Nombre"
        Me.ComboBox_Nombre.Size = New System.Drawing.Size(145, 21)
        Me.ComboBox_Nombre.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(27, 20)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(44, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Nombre"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Button_Modificar)
        Me.GroupBox2.Controls.Add(Me.Button_Eliminar)
        Me.GroupBox2.Controls.Add(Me.Button_Agregar)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.TextBox_SueldoLiquido)
        Me.GroupBox2.Controls.Add(Me.Button_CalcularSueldoLiquido)
        Me.GroupBox2.Controls.Add(Me.TextBox_Descuentos)
        Me.GroupBox2.Controls.Add(Me.TextBox_Bonos)
        Me.GroupBox2.Controls.Add(Me.TextBox_SueldoBase)
        Me.GroupBox2.Controls.Add(Me.TextBox_Rut)
        Me.GroupBox2.Location = New System.Drawing.Point(13, 120)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(245, 259)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Liquidacion"
        '
        'Button_Modificar
        '
        Me.Button_Modificar.Location = New System.Drawing.Point(94, 194)
        Me.Button_Modificar.Name = "Button_Modificar"
        Me.Button_Modificar.Size = New System.Drawing.Size(76, 23)
        Me.Button_Modificar.TabIndex = 14
        Me.Button_Modificar.Text = "Modificar"
        Me.Button_Modificar.UseVisualStyleBackColor = True
        '
        'Button_Eliminar
        '
        Me.Button_Eliminar.Location = New System.Drawing.Point(176, 194)
        Me.Button_Eliminar.Name = "Button_Eliminar"
        Me.Button_Eliminar.Size = New System.Drawing.Size(63, 23)
        Me.Button_Eliminar.TabIndex = 13
        Me.Button_Eliminar.Text = "Eliminar"
        Me.Button_Eliminar.UseVisualStyleBackColor = True
        '
        'Button_Agregar
        '
        Me.Button_Agregar.Location = New System.Drawing.Point(26, 194)
        Me.Button_Agregar.Name = "Button_Agregar"
        Me.Button_Agregar.Size = New System.Drawing.Size(64, 23)
        Me.Button_Agregar.TabIndex = 11
        Me.Button_Agregar.Text = "Agregar"
        Me.Button_Agregar.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(27, 27)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(24, 13)
        Me.Label7.TabIndex = 10
        Me.Label7.Text = "Rut"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(27, 54)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(67, 13)
        Me.Label6.TabIndex = 9
        Me.Label6.Text = "Sueldo Base"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(27, 81)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(37, 13)
        Me.Label5.TabIndex = 8
        Me.Label5.Text = "Bonos"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(27, 108)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(64, 13)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "Descuentos"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(27, 165)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(77, 13)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Sueldo Liquido"
        '
        'TextBox_SueldoLiquido
        '
        Me.TextBox_SueldoLiquido.Location = New System.Drawing.Point(138, 158)
        Me.TextBox_SueldoLiquido.Name = "TextBox_SueldoLiquido"
        Me.TextBox_SueldoLiquido.Size = New System.Drawing.Size(101, 20)
        Me.TextBox_SueldoLiquido.TabIndex = 5
        '
        'Button_CalcularSueldoLiquido
        '
        Me.Button_CalcularSueldoLiquido.Location = New System.Drawing.Point(30, 128)
        Me.Button_CalcularSueldoLiquido.Name = "Button_CalcularSueldoLiquido"
        Me.Button_CalcularSueldoLiquido.Size = New System.Drawing.Size(209, 23)
        Me.Button_CalcularSueldoLiquido.TabIndex = 4
        Me.Button_CalcularSueldoLiquido.Text = "Calcular Sueldo liquido"
        Me.Button_CalcularSueldoLiquido.UseVisualStyleBackColor = True
        '
        'TextBox_Descuentos
        '
        Me.TextBox_Descuentos.Location = New System.Drawing.Point(138, 101)
        Me.TextBox_Descuentos.Name = "TextBox_Descuentos"
        Me.TextBox_Descuentos.Size = New System.Drawing.Size(101, 20)
        Me.TextBox_Descuentos.TabIndex = 3
        '
        'TextBox_Bonos
        '
        Me.TextBox_Bonos.Location = New System.Drawing.Point(138, 74)
        Me.TextBox_Bonos.Name = "TextBox_Bonos"
        Me.TextBox_Bonos.Size = New System.Drawing.Size(101, 20)
        Me.TextBox_Bonos.TabIndex = 2
        '
        'TextBox_SueldoBase
        '
        Me.TextBox_SueldoBase.Location = New System.Drawing.Point(138, 47)
        Me.TextBox_SueldoBase.Name = "TextBox_SueldoBase"
        Me.TextBox_SueldoBase.Size = New System.Drawing.Size(101, 20)
        Me.TextBox_SueldoBase.TabIndex = 1
        '
        'TextBox_Rut
        '
        Me.TextBox_Rut.BackColor = System.Drawing.SystemColors.HighlightText
        Me.TextBox_Rut.Location = New System.Drawing.Point(138, 20)
        Me.TextBox_Rut.MaxLength = 12
        Me.TextBox_Rut.Name = "TextBox_Rut"
        Me.TextBox_Rut.ReadOnly = True
        Me.TextBox_Rut.Size = New System.Drawing.Size(101, 20)
        Me.TextBox_Rut.TabIndex = 0
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.DataGridView_Liquidaciones)
        Me.GroupBox3.Location = New System.Drawing.Point(264, 13)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(861, 366)
        Me.GroupBox3.TabIndex = 2
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Liquidaciones"
        '
        'DataGridView_Liquidaciones
        '
        Me.DataGridView_Liquidaciones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView_Liquidaciones.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView_Liquidaciones.Location = New System.Drawing.Point(3, 16)
        Me.DataGridView_Liquidaciones.Name = "DataGridView_Liquidaciones"
        Me.DataGridView_Liquidaciones.ReadOnly = True
        Me.DataGridView_Liquidaciones.Size = New System.Drawing.Size(855, 347)
        Me.DataGridView_Liquidaciones.TabIndex = 0
        '
        'Form_RRHH
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1206, 502)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "Form_RRHH"
        Me.Text = "RRHH"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        CType(Me.DataGridView_Liquidaciones, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Button_ObtenerDatosLiquidacion As Button
    Friend WithEvents Label2 As Label
    Friend WithEvents TextBox_Periodo As TextBox
    Friend WithEvents ComboBox_Nombre As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents Label7 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents TextBox_SueldoLiquido As TextBox
    Friend WithEvents Button_CalcularSueldoLiquido As Button
    Friend WithEvents TextBox_Descuentos As TextBox
    Friend WithEvents TextBox_Bonos As TextBox
    Friend WithEvents TextBox_SueldoBase As TextBox
    Friend WithEvents TextBox_Rut As TextBox
    Friend WithEvents Button_Agregar As Button
    Friend WithEvents Button_Eliminar As Button
    Friend WithEvents Button_Modificar As Button
    Friend WithEvents DataGridView_Liquidaciones As DataGridView
End Class
