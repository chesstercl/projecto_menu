﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form_Principal
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.TrabajadoresToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GestionarTrabajadoresToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LiquidacionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GenerarLiquidacionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SalirToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TerminarProgramaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ImpresionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TrabajadoresToolStripMenuItem, Me.LiquidacionesToolStripMenuItem, Me.SalirToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(390, 24)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'TrabajadoresToolStripMenuItem
        '
        Me.TrabajadoresToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.GestionarTrabajadoresToolStripMenuItem})
        Me.TrabajadoresToolStripMenuItem.Name = "TrabajadoresToolStripMenuItem"
        Me.TrabajadoresToolStripMenuItem.Size = New System.Drawing.Size(83, 20)
        Me.TrabajadoresToolStripMenuItem.Text = "Trabajadores"
        '
        'GestionarTrabajadoresToolStripMenuItem
        '
        Me.GestionarTrabajadoresToolStripMenuItem.Name = "GestionarTrabajadoresToolStripMenuItem"
        Me.GestionarTrabajadoresToolStripMenuItem.Size = New System.Drawing.Size(120, 22)
        Me.GestionarTrabajadoresToolStripMenuItem.Text = "Gestionar"
        '
        'LiquidacionesToolStripMenuItem
        '
        Me.LiquidacionesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.GenerarLiquidacionesToolStripMenuItem, Me.ImpresionToolStripMenuItem})
        Me.LiquidacionesToolStripMenuItem.Name = "LiquidacionesToolStripMenuItem"
        Me.LiquidacionesToolStripMenuItem.Size = New System.Drawing.Size(82, 20)
        Me.LiquidacionesToolStripMenuItem.Text = "Liquidaciones"
        '
        'GenerarLiquidacionesToolStripMenuItem
        '
        Me.GenerarLiquidacionesToolStripMenuItem.Name = "GenerarLiquidacionesToolStripMenuItem"
        Me.GenerarLiquidacionesToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.GenerarLiquidacionesToolStripMenuItem.Text = "Generar"
        '
        'SalirToolStripMenuItem
        '
        Me.SalirToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TerminarProgramaToolStripMenuItem})
        Me.SalirToolStripMenuItem.Name = "SalirToolStripMenuItem"
        Me.SalirToolStripMenuItem.Size = New System.Drawing.Size(39, 20)
        Me.SalirToolStripMenuItem.Text = "Salir"
        '
        'TerminarProgramaToolStripMenuItem
        '
        Me.TerminarProgramaToolStripMenuItem.Name = "TerminarProgramaToolStripMenuItem"
        Me.TerminarProgramaToolStripMenuItem.Size = New System.Drawing.Size(163, 22)
        Me.TerminarProgramaToolStripMenuItem.Text = "terminar Programa"
        '
        'ImpresionToolStripMenuItem
        '
        Me.ImpresionToolStripMenuItem.Name = "ImpresionToolStripMenuItem"
        Me.ImpresionToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.ImpresionToolStripMenuItem.Text = "Impresion"
        '
        'Form_Principal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(390, 100)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Name = "Form_Principal"
        Me.Text = "Principal"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents TrabajadoresToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents GestionarTrabajadoresToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents LiquidacionesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents SalirToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents GenerarLiquidacionesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents TerminarProgramaToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ImpresionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
End Class
