﻿Public Class Form_RRHH

    Public ruti As String

    Private Sub Form_RRHH_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Llenar(ComboBox_Nombre)

        Inicializar()
        DataGridView_Liquidaciones.DataSource = Llenar.Tables("liquidaciones")

    End Sub

    Private Sub Button_ObtenerDatos_Click(sender As Object, e As EventArgs) Handles Button_ObtenerDatosLiquidacion.Click
        If (TextBox_Periodo.Text = "AAAAMM") OrElse Len(TextBox_Periodo.Text) <> 6 Then
            MsgBox("Debe llenar el campo de periodo antes de poder realizar esta consulta")
        Else
            TextBox_Rut.Text = ruti
            Buscar(ruti, Integer.Parse(TextBox_Periodo.Text))

        End If

    End Sub

    Private Sub ComboBox_Nombre_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox_Nombre.SelectedIndexChanged
        If (ComboBox_Nombre.SelectedIndex > -1) Then
            BuscarRut(ComboBox_Nombre.Text)
        Else

        End If

    End Sub

    Private Sub Button_Agregar_Click(sender As Object, e As EventArgs) Handles Button_Agregar.Click

        If (TextBox_SueldoBase.Text <= 0 Or TextBox_Bonos.Text <= 0 Or TextBox_Descuentos.Text <= 0 Or TextBox_SueldoLiquido.Text <= 0) Then

            MsgBox("Uno o más campos se encuentran vacios, por favor, complete todos los campos antes de intentar ingresar.")
        Else
            InsertarLiquidacion(ruti, CInt(TextBox_Periodo.Text), CInt(TextBox_SueldoBase.Text), CInt(TextBox_Bonos.Text), CInt(TextBox_Descuentos.Text), CInt(TextBox_SueldoLiquido.Text))
            Llenargrid()
            Inicializar()
        End If

    End Sub

    Private Sub Button_CalcularSueldoLiquido_Click(sender As Object, e As EventArgs) Handles Button_CalcularSueldoLiquido.Click
        If (TextBox_SueldoBase.Text <= 0 Or TextBox_Bonos.Text <= 0 Or TextBox_Descuentos.Text <= 0) Then
            MsgBox("Los valores deben ser mayor a 0 para poder realizar está operación.")
        Else
            If (CInt(TextBox_Descuentos.Text) > CInt(TextBox_SueldoBase.Text)) Then
                MsgBox("Los descuentos no pueden ser mayor al sueldo base.")
            Else
                TextBox_SueldoLiquido.Text = ((CInt(TextBox_SueldoBase.Text) + CInt(TextBox_Bonos.Text)) - CInt(TextBox_Descuentos.Text))
            End If

        End If

    End Sub

    Sub Inicializar()
        TextBox_Descuentos.Text = 0
        TextBox_SueldoBase.Text = 0
        TextBox_SueldoLiquido.Text = 0
        TextBox_Bonos.Text = 0
        TextBox_Periodo.Text = ""
        TextBox_Rut.Text = ""
        TextBox_Periodo.Select()

    End Sub

    Private Sub Button_Modificar_Click(sender As Object, e As EventArgs) Handles Button_Modificar.Click
        If (TextBox_SueldoBase.Text.Length <= 0 Or TextBox_Bonos.Text <= 1 Or TextBox_Descuentos.Text <= 0 Or TextBox_SueldoLiquido.Text <= 0) Then
            MsgBox("Uno o más campos se encuentran vacios o incorrectos, por favor, complete y/o corrija los campos antes de intentar modificar.")
        Else
            ModificarLiquidacion(CInt(TextBox_SueldoBase.Text), CInt(TextBox_Bonos.Text), CInt(TextBox_Descuentos.Text), CInt(TextBox_SueldoLiquido.Text), ruti, TextBox_Periodo.Text)
            Llenargrid()
            Inicializar()
        End If

    End Sub

    Private Sub Button_Eliminar_Click(sender As Object, e As EventArgs) Handles Button_Eliminar.Click
        If (TextBox_Rut.Text = "") Then
            MsgBox("Debe ingresar el rut del trabajador para eliminar la liquidación")
        Else
            EliminarLiquidacion(ruti, TextBox_Periodo.Text)
            Llenargrid()
            Inicializar()
        End If

    End Sub

    Sub Llenargrid()
        DataGridView_Liquidaciones.DataSource = Llenar.Tables("liquidaciones")
    End Sub

    Private Sub TextBox_Periodo_GotFocus(sender As Object, e As EventArgs) Handles TextBox_Periodo.GotFocus
        If TextBox_Periodo.Text = "AAAAMM" Then
            TextBox_Periodo.ForeColor = Color.Black
            TextBox_Periodo.Text = ""
        End If
    End Sub

    Private Sub TextBox_Periodo_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox_Periodo.KeyPress
        If (InStr("1234567890" & Chr(8), e.KeyChar) = 0) Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub TextBox_Periodo_LostFocus(sender As Object, e As EventArgs) Handles TextBox_Periodo.LostFocus

        If TextBox_Periodo.Text = "" Then
            TextBox_Periodo.ForeColor = Color.Gray
            TextBox_Periodo.Text = "AAAAMM"
        End If
    End Sub

    Private Sub TextBox_SueldoBase_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox_SueldoBase.KeyPress
        If (InStr("1234567890" & Chr(8), e.KeyChar) = 0) Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub TextBox_Bonos_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox_Bonos.KeyPress
        If (InStr("1234567890" & Chr(8), e.KeyChar) = 0) Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub TextBox_Descuentos_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox_Descuentos.KeyPress
        If (InStr("1234567890" & Chr(8), e.KeyChar) = 0) Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub TextBoxSueldoLiquido_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox_SueldoLiquido.KeyPress
        If (InStr("1234567890" & Chr(8), e.KeyChar) = 0) Then
            e.KeyChar = ""
        End If
    End Sub

End Class
