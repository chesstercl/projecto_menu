﻿Public Class Form_Trabajadores

    Dim estado_trabajador() As String = {"activo", "inactivo"}

    Private Sub Form_Trabajadores_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Llenargrid()
        Cargar_estados()
    End Sub

    Sub Cargar_estados()
        For index = 0 To 1
            ComboBox_Estado.Items.Add(estado_trabajador(index))
        Next
    End Sub

    Private Sub Button_Buscar_Click(sender As Object, e As EventArgs) Handles Button_Buscar.Click
        If (TextBox_Rut.Text.Equals("")) Then
            MsgBox("Debe ingresar un rut para realizar la busqueda")
        Else
            BuscarTrabajador(TextBox_Rut.Text)
        End If
    End Sub

    Private Sub Button_Agregar_Click(sender As Object, e As EventArgs) Handles Button_Agregar.Click
        If (TextBox_Nombre.Text.Equals("") OrElse TextBox_Rut.Text.Equals("") OrElse NumericUD_SueldoBase.Value = 0 OrElse ComboBox_Estado.Text = "") Then
            MsgBox("Todos los campos deben contener valores")
        Else
            InsertarTrabajador(TextBox_Nombre.Text, TextBox_Rut.Text, CInt(NumericUD_SueldoBase.Value), ComboBox_Estado.Text)
            Llenargrid()
            Limpiar()
        End If
    End Sub

    Private Sub Button_Modificar_Click(sender As Object, e As EventArgs) Handles Button_Modificar.Click
        If (TextBox_Rut.Text = "") Then
            MessageBox.Show("Debe ingresar un rut para realizar una modificacion")
        Else
            ModificarTrabajador(TextBox_Nombre.Text, ComboBox_Estado.SelectedItem.ToString, CInt(NumericUD_SueldoBase.Value), TextBox_Rut.Text)
            Llenargrid()
            Limpiar()
        End If
    End Sub

    Private Sub Button_Eliminar_Click(sender As Object, e As EventArgs) Handles Button_Eliminar.Click
        If (TextBox_Rut.Text = "") Then
            MessageBox.Show("Debe ingresar un rut para realizar una modificacion")
        Else
            EliminarTrabajador(TextBox_Rut.Text)
            Llenargrid()
            Limpiar()
        End If
        
    End Sub

    Sub Llenargrid()
        DataGridView_Trabajadores.DataSource = Llenar2.Tables("trabajadores")
    End Sub

    Sub Limpiar()
        TextBox_Nombre.Text = ""
        TextBox_Rut.Text = ""
        ComboBox_Estado.SelectedIndex = 0
        NumericUD_SueldoBase.Value = 0
    End Sub

    Private Sub ValidarRut(sender As Object, e As KeyPressEventArgs) Handles TextBox_Rut.KeyPress
        If (InStr("1234567890-" & Chr(8), e.KeyChar) = 0) Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub ValidarNombre(sender As Object, e As KeyPressEventArgs) Handles TextBox_Nombre.KeyPress
        If Char.IsLetter(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsSeparator(e.KeyChar) Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub

End Class