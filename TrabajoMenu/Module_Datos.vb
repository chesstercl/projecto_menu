﻿Imports MySql.Data.MySqlClient

Module Modulo

    Dim oConexion As New MySqlConnection
    Dim dt As New DataTable
    Dim oDatatable As New DataTable
    Dim oDataAdapter As New MySqlDataAdapter
    Dim oDataset As New DataSet
    Dim oSql As String
    Dim sqlCommand As New MySqlCommand
    Dim dtReader As MySqlDataReader

    Dim c = "server=localhost;database=bd_trabajadores;user id=root;password=;"

    Sub Llenar(ByRef ComboBox_Nombre As ComboBox) 
        oConexion.ConnectionString = c
        oSql = "SELECT * FROM trabajadores"
        oDataAdapter = New MySqlDataAdapter(oSql, oConexion)
        oDataset.Tables.Clear()
        oDataset.Tables.Add("nombre")

        oDataAdapter.Fill(oDataset.Tables("nombre"))
        ComboBox_Nombre.DataSource = oDataset.Tables("nombre")
        ComboBox_Nombre.DisplayMember = "nombre"
        oConexion.Close()

    End Sub


    Sub Buscar(ByRef rut As String, ByRef period As Integer)

        oConexion.Close()
        oConexion.Open()

        Dim periodo As Integer
        Dim sueldo_base As Integer
        Dim bono As Integer
        Dim descuento As Integer
        Dim liquido As Integer
        Dim ruti As String


        oSql = ("SELECT * FROM liquidaciones WHERE rut_trabajador='" & rut & "' AND periodo='" & period & "'")

        Try

            Dim comando As New MySqlCommand(oSql, oConexion)

            Dim lee As MySqlDataReader = comando.ExecuteReader

            If (lee.Read = False) Then
                Form_RRHH.TextBox_Rut.Text = ""
                MsgBox("No se han encontrado datos relacionados a ese trabajador")
                Form_RRHH.TextBox_Rut.Text = Form_RRHH.ruti

                oConexion.Close()

            Else

                ruti = lee.Item("rut_trabajador")
                Form_RRHH.TextBox_Rut.Text = ruti

                periodo = lee.Item("periodo")
                Form_RRHH.TextBox_Periodo.Text = periodo

                sueldo_base = lee.Item("sueldo_base")
                Form_RRHH.TextBox_SueldoBase.Text = sueldo_base

                bono = lee.Item("bonos")
                Form_RRHH.TextBox_Bonos.Text = bono

                descuento = lee.Item("descuentos")
                Form_RRHH.TextBox_Descuentos.Text = descuento

                liquido = lee.Item("sueldo_liquido")
                Form_RRHH.TextBox_SueldoLiquido.Text = liquido

                MsgBox("Se han encontrado datos")

            End If
        Catch ex As Exception

            MsgBox(ex.ToString)

        End Try
        oConexion.Close()

    End Sub



    Sub BuscarRut(ByRef nombre As String)
        Dim rut As String

        oConexion.Open()

        oSql = ("SELECT rut FROM trabajadores WHERE nombre='" & nombre & "'")

        Try

            Dim comando As New MySqlCommand(oSql, oConexion)

            Dim lee As MySqlDataReader = comando.ExecuteReader

            If (lee.Read = False) Then


            Else
                rut = lee.Item("rut")
                Form_RRHH.ruti = rut
                'Form1.txtRut1.Text = rut

            End If
        Catch ex As Exception

            MsgBox(ex.ToString)
        End Try
        oConexion.Close()
    End Sub


    Public Function InsertarLiquidacion(ByVal rut As String, ByVal periodo As Integer, ByVal sueldo_base As Integer, ByVal bonos As Integer, ByVal descuentos As Integer, ByVal liquido As Integer)
        oConexion.Close()
        oConexion.Open()

        Dim resultado As String
        Try
            Dim comando As New MySqlCommand("INSERT INTO liquidaciones(rut_trabajador,periodo,sueldo_base,bonos,descuentos,sueldo_liquido) VALUES ('" & rut & "','" & periodo & "','" & sueldo_base & "','" & bonos & "','" & descuentos & "','" & liquido & "')", oConexion)
            comando.ExecuteNonQuery()
            resultado = "Se insertaron correctamente los datos"
            MsgBox(resultado)
            oConexion.Close()


        Catch ex As Exception
            resultado = "No se pudieron añadir los datos" + ex.ToString()
        End Try

        Return resultado
        oConexion.Close()
    End Function

    Public Function ModificarLiquidacion(ByVal sueldo_base As Integer, ByVal bonos As Integer, ByVal descuentos As Integer, ByVal liquido As Integer, ByRef rut As String, ByRef period As Integer)
        oConexion.Close()
        oConexion.Open()
        Dim resultado As String
        Try
            Dim comando As New MySqlCommand("UPDATE liquidaciones SET sueldo_base='" & sueldo_base & "',bonos='" & bonos & "',descuentos='" & descuentos & "',sueldo_liquido='" & liquido & "' WHERE liquidaciones.rut_trabajador='" & rut & "'AND liquidaciones.periodo='" & period & "'", oConexion)
            comando.ExecuteNonQuery()
            resultado = "Se actualizaron correctamente los datos"
            oConexion.Close()
            MsgBox(resultado)
        Catch ex As Exception
            resultado = "No se pudieron añadir los datos" + ex.ToString()
        Finally
            If (ConnectionState.Open = True) Then
                oConexion.Close()
            End If
        End Try
        Return resultado

    End Function

    Public Function EliminarLiquidacion(ByRef rut As String, ByRef period As Integer)
        oConexion.Open()
        Dim resultado As String
        Try
            Dim comando As New MySqlCommand("DELETE FROM liquidaciones WHERE rut_trabajador='" & rut & "'AND periodo='" & period & "'", oConexion)
            comando.ExecuteNonQuery()
            resultado = "Se eliminaron correctamente los datos"
            MsgBox(resultado)
            oConexion.Close()

            Limpiar()
        Catch ex As Exception
            resultado = "No se pudieron eliminar los datos" + ex.ToString()
        End Try
        Return resultado

    End Function

    Sub Limpiar()
        Form_RRHH.TextBox_Rut.Text = ""
        Form_RRHH.TextBox_Periodo.Text = ""
        Form_RRHH.TextBox_Bonos.Text = 0
        Form_RRHH.TextBox_Descuentos.Text = 0
        Form_RRHH.TextBox_SueldoBase.Text = 0
        Form_RRHH.TextBox_SueldoLiquido.Text = 0

    End Sub


    Public Function InsertarTrabajador(ByVal rut As String, ByVal nombre As String, ByVal estado As String, ByVal sueldo_base As Integer)

        Dim resultado As String
        Try

            oConexion.ConnectionString = c
            oConexion.Open()
            Dim comando As New MySqlCommand("INSERT INTO trabajadores(rut,nombre,estado,sueldo_base) VALUES ('" & rut & "','" & nombre & "','" & estado & "','" & sueldo_base & "')", oConexion)
            comando.ExecuteNonQuery()
            resultado = "Se insertaron correctamente los datos"
            MsgBox(resultado)



        Catch ex As Exception
            resultado = "No se pudieron añadir los datos" + ex.ToString()
        Finally
            If (ConnectionState.Open = True) Then
                oConexion.Close()
            End If
        End Try

        Return resultado

    End Function


    Public Function ModificarTrabajador(ByVal nombre As String, ByVal estado As String, ByVal sueldo As Integer, ByRef rut As String)

        Dim resultado As String
        Try
            oConexion.ConnectionString = c
            oConexion.Open()
            Dim comando As New MySqlCommand("UPDATE trabajadores SET nombre='" & nombre & "',estado='" & estado & "',sueldo_base='" & sueldo & "' WHERE rut='" & rut & "'", oConexion)
            comando.ExecuteNonQuery()
            resultado = "Se actualizaron correctamente los datos"
            MsgBox(resultado)

        Catch ex As Exception
            resultado = "No se pudieron añadir los datos" + ex.ToString()
        Finally
            If (ConnectionState.Open = True) Then
                oConexion.Close()
            End If
        End Try

        Return resultado

    End Function

    Public Function EliminarTrabajador(ByRef rut As String)

        Dim resultado As String
        Try
            oConexion.ConnectionString = c
            oConexion.Open()
            Dim comando As New MySqlCommand("DELETE FROM trabajadores WHERE rut='" & rut & "'", oConexion)
            comando.ExecuteNonQuery()
            resultado = "Se eliminaron correctamente los datos"
            MsgBox(resultado)
            oConexion.Close()

            Limpiar()
        Catch ex As Exception
            resultado = "No se pudieron eliminar los datos" + ex.ToString()
        Finally
            If (ConnectionState.Open = True) Then
                oConexion.Close()
            End If
        End Try
        Return resultado

    End Function


    Sub BuscarTrabajador(ByRef rut As String)
        oSql = ("SELECT * FROM trabajadores WHERE rut='" & rut & "'")
        Try
            oConexion.ConnectionString = c
            oConexion.Open()
            Dim comando As New MySqlCommand(oSql, oConexion)
            Dim lee As MySqlDataReader = comando.ExecuteReader

            If (lee.Read = False) Then
                MsgBox("El rut que está ingresando no se encuentra")
            Else
                Dim nombre As String
                Dim base As Integer
                Dim estado As String

                nombre = lee.Item("nombre")
                Form_Trabajadores.TextBox_Nombre.Text = nombre
                base = lee.Item("sueldo_base")
                Form_Trabajadores.NumericUD_SueldoBase.Value = base
                estado = lee.Item("estado")
                Form_Trabajadores.ComboBox_Estado.SelectedItem = estado
            End If
        Catch ex As Exception

            MsgBox(ex.ToString)
        End Try
        oConexion.Close()
    End Sub


    Function Llenar() As DataSet
        oConexion.ConnectionString = c

        oSql = "SELECT l.periodo, t.nombre, t.rut,l.sueldo_base,l.bonos,l.descuentos,l.sueldo_liquido FROM liquidaciones l, trabajadores t WHERE l.rut_trabajador=t.rut"
        oDataAdapter = New MySqlDataAdapter(oSql, oConexion)
        oDataset.Tables.Clear()

        oDataAdapter.Fill(oDataset, "liquidaciones")
        oDataset.Tables(0).Columns(0).ColumnName = "PERIODO"
        oDataset.Tables(0).Columns(1).ColumnName = "NOMBRE"
        oDataset.Tables(0).Columns(2).ColumnName = "RUT"
        oDataset.Tables(0).Columns(3).ColumnName = "SUELDO BASE"
        oDataset.Tables(0).Columns(4).ColumnName = "BONOS"
        oDataset.Tables(0).Columns(5).ColumnName = "DESCUENTOS"
        oDataset.Tables(0).Columns(6).ColumnName = "SUELDO LIQUIDO"
        oConexion.Close()

        Return oDataset
    End Function

    Function Llenar2() As DataSet
        oConexion.Close()
        oConexion.ConnectionString = c
        Try
            oConexion.Open()
            oSql = "SELECT * FROM trabajadores"
            oDataAdapter = New MySqlDataAdapter(oSql, oConexion)
            oDataset.Tables.Clear()
            oDataAdapter.Fill(oDataset, "trabajadores")
            oDataset.Tables(0).Columns(0).ColumnName = "RUT"
            oDataset.Tables(0).Columns(1).ColumnName = "NOMBRE"
            oDataset.Tables(0).Columns(2).ColumnName = "ESTADO"
            oDataset.Tables(0).Columns(3).ColumnName = "SUELDO BASE"
            oConexion.Close()
        Catch ex As Exception
            MsgBox(ex.ToString())
        End Try
        Return oDataset
    End Function
    Function PorPeriodo(ByRef periodo As String) As DataSet

        oConexion.ConnectionString = c
        oSql = "SELECT t.nombre, t.rut,t.sueldo_base,l.bonos,l.descuentos,l.sueldo_liquido FROM liquidaciones l, trabajadores t WHERE l.rut_trabajador=t.rut AND l.periodo='" & periodo & "'"
        oDataAdapter = New MySqlDataAdapter(oSql, oConexion)
        oDataset.Tables.Clear()
        oDataAdapter.Fill(oDataset)
        oDataset.Tables(0).Columns(0).ColumnName = "NOMBRE"
        oDataset.Tables(0).Columns(1).ColumnName = "RUT"
        oDataset.Tables(0).Columns(2).ColumnName = "SUELDO BASE"
        oDataset.Tables(0).Columns(3).ColumnName = "BONOS"
        oDataset.Tables(0).Columns(4).ColumnName = "DESCUENTOS"
        oDataset.Tables(0).Columns(5).ColumnName = "SUELDO LIQUIDO"
        oConexion.Close()
        Return oDataset
    End Function

    Function Periodo(ByRef periodo1 As String)

        oConexion.ConnectionString = c
        oConexion.Open()
        oSql = "SELECT t.nombre, t.rut,t.sueldo_base,l.bonos,l.descuentos,l.sueldo_liquido FROM liquidaciones l, trabajadores t WHERE l.rut_trabajador=t.rut AND l.periodo='" & periodo1 & "'"
        With sqlCommand
            .CommandText = oSql
            .Connection = oConexion
        End With
        With oDataAdapter
            .SelectCommand = sqlCommand
            .Fill(oDatatable)
        End With
        For i = 0 To oDatatable.Rows.Count - 1
            With Form_Impresion.ListView1
                .Items.Add(oDatatable.Rows(i)("nombre"))
                With .Items(.Items.Count - 1).SubItems
                    .Add(oDatatable.Rows(i)("rut"))
                    .Add(oDatatable.Rows(i)("sueldo_base"))
                    .Add(oDatatable.Rows(i)("bonos"))
                    .Add(oDatatable.Rows(i)("descuentos"))
                    .Add(oDatatable.Rows(i)("sueldo_liquido"))
                End With
            End With
        Next
        oConexion.Close()
        Return oDataset
    End Function
End Module
